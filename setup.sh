#!/bin/sh

# setting server
pushd tinyms
mkdir tmp
mkdir .credentials
mkdir files
pushd files
mkdir data
mkdir media
popd
popd

# setting client
pushd tinyms-client
mkdir .credentials
popd