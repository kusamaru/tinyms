use std::{collections::HashSet, process::ExitCode, path::PathBuf};

use anyhow::{Result, Context};
use async_recursion::async_recursion;
use enclose::enclose;
use futures::{future::{Fuse, FusedFuture}, FutureExt as _};
use makiko::{Client, ClientConfig, bytes::Buf};
use once_cell::sync::OnceCell;
use tokio::{net::TcpStream, fs, io::{AsyncReadExt as _, AsyncWriteExt as _}, sync::mpsc::{self, Receiver}};

use crate::{structs::{HostFile, TaskHandle, Key, RunArgs, FileAttribute}, sftp};

static FLAG_ASK_YES_NO: OnceCell<bool> = OnceCell::new();

pub async fn run(args: RunArgs) -> Result<ExitCode> {
    let host = args.host.as_str();
    let port = args.port;
    let known_hosts = String::from("known_hosts");
    let _ = FLAG_ASK_YES_NO.set(args.ask_yes_no);

    println!("Connecting to {}:{}...", host, port);
    let stream = TcpStream::connect((host, port)).await?;
    let (client, client_rx, client_fut) = Client::open(stream, ClientConfig::default()).unwrap();
    let client_task = TaskHandle(tokio::task::spawn(client_fut));

    let host_name = makiko::host_file::host_port_to_hostname(host, port);
    let host_file = read_host_file(known_hosts).await?;
    let event_task = TaskHandle(tokio::task::spawn(
        run_events(client.clone(), client_rx, host_name, host_file)
    ));

    let path = std::path::Path::new("./.credentials/client_id_ed25519").to_path_buf();
    let key = {
        let data = fs::read(&path).await?;
        let decoded = makiko::keys::decode_pem_privkey_nopass(&data)?;
        Key { path: path.into(), data, decoded }
    };
    let interact_task = TaskHandle(tokio::task::spawn(enclose!{(client) async move {
        authenticate(&client, "client".into(), key).await.unwrap();

        let session_task = TaskHandle(tokio::task::spawn(enclose!{(client) async move {
            run_session(&client, args.files).await
        }}));

        let mut session_fut = session_task.fuse();

        let mut exit_code = ExitCode::SUCCESS;
        while !session_fut.is_terminated() {
            tokio::select! {
                res = &mut session_fut => exit_code = res?,
            }
        }

        client.disconnect(makiko::DisconnectError::by_app())?;
        Result::<_>::Ok(exit_code)
    }}));

    let mut client_fut = client_task.fuse();
    let mut event_fut = event_task.fuse();
    let mut interact_fut = interact_task.fuse();

    let mut exit_code = None;
    loop {
        if client_fut.is_terminated() && exit_code.is_some() {
            return Ok(exit_code.unwrap())
        }

        tokio::select! {
            res = &mut client_fut => res?,
            res = &mut event_fut => res?,
            res = &mut interact_fut => exit_code = Some(res?),
        }
    }
}

async fn read_host_file(known_hosts: String) -> Result<Option<HostFile>> {
    use std::io::Read;

    let path = std::path::Path::new(&known_hosts).to_path_buf();
    let mut file = std::fs::File::options()
        .create(true)
        .write(true) // for avoiding some errors
        .read(true)
        .open(&path)
        .unwrap();
    let mut file_data: Vec<u8> = Vec::new();
    file.read_to_end(&mut file_data)?;
    let decoded = makiko::host_file::File::decode(file_data.into());
    Ok(Some(HostFile { path, file: decoded }))
}

async fn run_events(
    client: Client,
    mut client_rx: makiko::ClientReceiver,
    host_name: String,
    mut host_file: Option<HostFile>
) -> Result<()> {
    let mut pubkey_task = Fuse::terminated();
    loop {
        tokio::select! {
            event = client_rx.recv() => match event? {
                Some(makiko::ClientEvent::ServerPubkey(pubkey, accept_tx)) => {
                    let client = client.clone();
                    let host_name = host_name.clone();
                    let host_file = host_file.take();
                    pubkey_task = TaskHandle(tokio::task::spawn(async move {
                        if verify_pubkey(pubkey, host_name, host_file).await? {
                            accept_tx.accept();
                        } else {
                            client.disconnect(makiko::DisconnectError {
                                reason_code: makiko::codes::disconnect::HOST_KEY_NOT_VERIFIABLE,
                                description: "user did not accept the host public key".to_string(),
                                description_lang: String::new(),
                            })?;
                        }
                        Result::<()>::Ok(())
                    })).fuse();
                },
                Some(_) => continue,
                None => break,
            },
            res = &mut pubkey_task => res?,
        }
    }
    Ok(())
}

async fn verify_pubkey(
    pubkey: makiko::Pubkey,
    host_name: String,
    mut host_file: Option<HostFile>,
) -> Result<bool> {
    if let Some(host_file) = host_file.as_ref() {
        use makiko::host_file::KeyMatch;
        match host_file.file.match_hostname_key(&host_name, &pubkey) {
            KeyMatch::Accepted(_entries) => {
                log::info!("pubkey found in {}", host_file.path.display());
                return Ok(true)
            },
            KeyMatch::Revoked(_entry) => {
                log::info!("pubkey revoked");
                return Ok(false)
            },
            KeyMatch::OtherKeys(_entries) => {
                log::info!("other keys");
                return Ok(false)
            },
            KeyMatch::NotFound => {
                log::info!("key was not found");
            },
        }
    }

    let prompt = format!(
        "ssh: server {:?} has pubkey with fingerprint {}\n ssh: do you want to connect?",
        host_name, pubkey.fingerprint()
    );

    if ask_yes_no(&prompt).await? {
        if let Some(host_file) = host_file.as_mut() {
            host_file.file.append_entry(makiko::host_file::File::entry_builder()
                .hostname(&host_name)
                .key(pubkey));
            fs::write(&host_file.path, &host_file.file.encode())
                .await
                .context(format!("could not write to {}", host_file.path.display()))?;
        }
        Ok(true)
    } else {
        Ok(false)
    }
}

async fn authenticate(
    client: &Client,
    username: String,
    mut key: Key
) -> Result<()> {
    struct AuthCtx<'c> {
        client: &'c Client,
        username: String,
        methods: HashSet<String>,
        pubkey_algo_names: Option<HashSet<String>>,
    }

    fn update_methods(ctx: &mut AuthCtx, failure: makiko::AuthFailure) {
        log::info!("auth methods that can continue: {:?}", failure.methods_can_continue);
        ctx.methods = failure.methods_can_continue.into_iter().collect();
    }

    fn update_pubkey_algo_names(ctx: &mut AuthCtx) -> Result<()> {
        ctx.pubkey_algo_names = ctx.client
            .auth_pubkey_algo_names()?
            .map(|names| names.into_iter().collect::<HashSet<_>>());
        if let Some(names) = ctx.pubkey_algo_names.as_ref() {
            log::info!("server supports these public key algorithms: {:?}", names);
        } 
        Ok(())
    }

    async fn try_auth_none(ctx: &mut AuthCtx<'_>) -> Result<bool> {
        log::info!("trying 'none' auth (must fail)");
        match ctx.client.auth_none(ctx.username.clone()).await? {
            makiko::AuthNoneResult::Success => anyhow::bail!("none auth success, not expected"),
            makiko::AuthNoneResult::Failure(failure) => update_methods(ctx, failure),
        }
        Ok(false)
    }

    async fn try_auth_key(ctx: &mut AuthCtx<'_>, key: &mut Key) -> Result<bool> {
        if !ctx.methods.contains("publickey") {
            log::error!("unsupported.");
            return Ok(false)
        }

        let pubkey = decode_pubkey(key).await?;
        for algo in pubkey.algos_compatible_less_secure().iter() {
            log::info!("algo: {:?}", algo);
            if let Some(names) = ctx.pubkey_algo_names.as_ref() {
                if !names.contains(algo.name) {
                    continue
                }
            }

            if try_auth_key_algo(ctx, key, &pubkey, algo).await? {
                log::info!("algo: {:?} = OK!!", algo);
                return Ok(true)
            }
        }
        Ok(false)
    }

    async fn try_auth_key_algo(
        ctx: &mut AuthCtx<'_>,
        key: &mut Key,
        pubkey: &makiko::Pubkey,
        algo: &'static makiko::PubkeyAlgo,
    ) -> Result<bool> {
        if !ctx.client.check_pubkey(ctx.username.clone(), &pubkey, algo).await? {
            return Ok(false)
        }

        let privkey = decode_privkey(key).await?;
        match ctx.client.auth_pubkey(ctx.username.clone(), privkey, algo).await? {
            makiko::AuthPubkeyResult::Success => return Ok(true),
            makiko::AuthPubkeyResult::Failure(failure) => update_methods(ctx, failure),
        }
        Ok(false)
    }

    async fn decode_pubkey(key: &mut Key) -> Result<makiko::Pubkey> {
        use makiko::keys::DecodedPrivkeyNopass;
        match &key.decoded {
            DecodedPrivkeyNopass::Privkey(privkey) => return Ok(privkey.pubkey()),
            DecodedPrivkeyNopass::Pubkey(pubkey) => return Ok(pubkey.clone()),
            _ => {},
        }
        decode_privkey(key).await.map(|privkey| privkey.pubkey())
    }

    async fn decode_privkey(key: &mut Key) -> Result<makiko::Privkey> {
        use makiko::keys::DecodedPrivkeyNopass;
        loop {
            if let DecodedPrivkeyNopass::Privkey(ref privkey) = key.decoded {
                return Ok(privkey.clone())
            }

            // todo!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            let _prompt = format!("ssh: passphrase for key {}", key.path.display());
            let passphrase = /* ask_for_passphrase(&_prompt).await?; */ String::new();
            match makiko::keys::decode_pem_privkey(&key.data, passphrase.as_bytes()) {
                Ok(privkey) => {
                    key.decoded = DecodedPrivkeyNopass::Privkey(privkey.clone());
                    return Ok(privkey)
                },
                Err(makiko::Error::BadKeyPassphrase) => {
                    println!("Passphrase not match.");
                    return Err(anyhow::anyhow!("passphrase fails"))
                },
                Err(err) => return Err(err.into()), 
            }
        }
    }

    let mut ctx = AuthCtx {
        client,
        username,
        methods: HashSet::new(),
        pubkey_algo_names: None,
    };

    try_auth_none(&mut ctx).await?;
    update_pubkey_algo_names(&mut ctx)?;
    if try_auth_key(&mut ctx, &mut key).await? { 
        println!("Authorization success");
        return Ok(()) 
    }

    eprintln!("auth fail. {:?}, {:?}", ctx.methods, key.path);
    anyhow::bail!("Authentication fail");
}

async fn run_session(
    client: &Client, 
    files: Vec<PathBuf>, 
) -> Result<ExitCode> {
    let config = makiko::ChannelConfig::default();
    let (session, mut session_rx) = client.open_session(config).await?;
    session.subsystem("sftp")?.wait().await?;

    let (reply, mut reply_rx) = mpsc::channel(1);

    let recv_task = tokio::task::spawn(async move {
        let mut stderr = tokio::io::stderr();
        while let Some(event) = session_rx.recv().await? {
            match event {
                makiko::SessionEvent::StdoutData(data) => {
                    // data output
                    let mut decoded = makiko::PacketDecode::new(data);
                    let _len = decoded.get_u32()?;
                    let byte = decoded.get_raw(1)?.get_int(1) as isize;
                    
                    use sftp::PacketType::*;
                    match byte {
                        x if x == SSH_FXP_VERSION as isize => {
                            log::info!("protocol version = {}", decoded.get_u32()?);
                        },
                        x if x == SSH_FXP_HANDLE as isize => {
                            let request_id = decoded.get_u32()?;
                            let handle_str = decoded.get_string()?;
                            reply.send(handle_str.clone()).await?;
                            log::info!("request_id: {}, handle: {}", request_id, handle_str);
                        },
                        x if x == SSH_FXP_STATUS as isize => {
                            let request_id = decoded.get_u32()?;
                            let status_code = decoded.get_u32()?;
                            // status_code is not SSH_FX_OK
                            if status_code != 0 {
                                let message = decoded.get_string()?;
                                log::warn!(
                                    "request_id: {}, status_code: {}, message: {}", 
                                    request_id, status_code, message
                                );
                            } else {
                                // Ok
                                // send status
                                reply.send(String::new()).await?;
                            }
                        },
                        x if x == SSH_FXP_EXTENDED_REPLY as isize => {
                            let _request_id = decoded.get_u32()?;
                            let request_type = decoded.get_string()?;
                            match request_type.as_str() {
                                "GET_FILELINK" => {
                                    let link = decoded.get_string()?;
                                    reply.send(link).await?;
                                },
                                _ => ()
                            }
                        },
                        _ => {}
                    }
                },
                makiko::SessionEvent::StderrData(data) => {
                    stderr.write_all(&data).await?;
                    stderr.flush().await?;
                },
                makiko::SessionEvent::ExitStatus(status) => {
                    log::info!("exit status {}", status);
                    return Ok(ExitCode::from(status as u8))
                },
                makiko::SessionEvent::ExitSignal(signal) => {
                    log::info!("exit signal {:?}", signal.signal_name);
                    let msg = format!("remote process exited with signal {:?}: {:?}\n", signal.signal_name, signal.message);
                    stderr.write_all(msg.as_bytes()).await?;
                    return Ok(ExitCode::from(255))
                },
                _ => {},
            }
        }
        anyhow::bail!("session terminated");
    });

    let send_task = tokio::task::spawn(enclose!{(session) async move {
        // INIT
        sftp::init(&session).await?;

        let file_dests: Vec<(String, String)> = upload_task(&session, &mut reply_rx, files).await?;
        file_dests.iter().for_each(|(handle, dest)| println!("File {} is available on {}.", handle, dest));
        session.close()?;
        Result::<ExitCode>::Ok(ExitCode::SUCCESS)
    }});

    let mut recv_fut = TaskHandle(recv_task);
    let mut send_fut = TaskHandle(send_task);
    loop {
        tokio::select! {
            recv_res = &mut recv_fut => {
                // if let Some(tio) = orig_tio {
                //     // leave raw mode 
                // }
                log::info!("recv_res: {:?}", recv_res);
                return recv_res
            },
            send_res = &mut send_fut => return send_res,
        };
    }
}

#[async_recursion]
async fn upload_task(
    session: &makiko::Session,
    receiver: &mut Receiver<String>,
    files: Vec<PathBuf>
) -> Result<Vec<(String, String)>> {
    let mut result: Vec<(String, String)> = Vec::new();
    for f in files {
        if f.is_dir() {
            // recursive
            let new = std::fs::read_dir(f)?
                .filter_map(|entry| entry.ok())
                .map(|entry| entry.path())
                .collect::<Vec<_>>();

            result.append(
                &mut upload_task(
                    &session, 
                    receiver, 
                    new
                ).await?
            )
        } else {
            // OPEN
            let Some(file_name) = f.file_name() else { anyhow::bail!("fail to open file {}", f.to_string_lossy().to_string()) };
            let handle_name = format!("/{}", file_name.to_string_lossy().to_string());
            let request_id = 127;
            sftp::open(&session, request_id, handle_name, FileAttribute { size: None, file_type: None }).await?;

            // WRITE
            println!("Uploading: {}", file_name.to_string_lossy().to_string());
            let Some(handle) = receiver.recv().await else { anyhow::bail!("could not read SSH_FXP_HANDLE packet") };
            let mut file = tokio::fs::OpenOptions::new().read(true).open(f.clone()).await?;
            let read_len = 32768;
            let mut offset = 0;
            while let Some(buf) = read_to(&mut file, read_len).await {
                let len = buf.len() as u64;
                sftp::write(&session, request_id, handle.clone(), offset, buf).await?;
                offset += len;
                receiver.recv().await;
            }

            // CLOSE
            sftp::close(&session, request_id, handle.clone()).await?;
            receiver.recv().await;
            println!("File {} was successfully uploaded.", handle);

            // EXTENDED
            // get result
            sftp::extended(&session, request_id, handle.clone()).await?;
            let Some(dest) = receiver.recv().await else { eprintln!("error: could not find file destination in SSH_FXP_EXTENDED."); anyhow::bail!(""); };

            result.push((f.to_string_lossy().to_string().replace('\\', "/"), dest));
        }
    }   

    Ok(result)
}

async fn ask_yes_no(prompt: &str) -> Result<bool> {
    // skip if the flag is enabled
    if let Some(false) = FLAG_ASK_YES_NO.get() { return Ok(true) }

    let mut stdout = tokio::io::stdout();
    stdout.write_all(format!("{} [y/N]: ", prompt).as_bytes()).await?;
    stdout.flush().await?;

    let mut stdin = tokio::io::stdin();
    let mut yes = false;
    loop {
        let c = stdin.read_u8().await?;
        if c == b'\r' || c == b'\n' {
            break
        } else if c.is_ascii_whitespace() {
            continue
        } else if c == b'y' || c == b'Y' {
            yes = true;
        } else {
            yes = false;
        }
    }

    Ok(yes)
}

async fn read_to(file: &mut tokio::fs::File, len: u64) -> Option<Vec<u8>> {
    let mut buf: Vec<u8> = Vec::new();
    
    let mut index: usize = 0;
    while index < len as usize {
        let mut b = vec![0u8; (len - index as u64) as usize];
        let result = file.read(&mut b).await;
        match result {
            Ok(0) => { // EOF or b.len = 0
                break;
            },
            Ok(n) => { // read N byte(s)
                let tmp = &b[0..n];
                buf.append(&mut tmp.to_vec());
                index += n;
            },
            Err(_) => return None,
        }
    }
    if index == 0 { 
        // index = len = 0
        return None 
    }
    if index < len as usize {
        // maybe file last part(buffer was not filled)
        let tmp = &buf[0..index];
        Some(tmp.to_vec())
    } else {
        Some(buf)
    }
}