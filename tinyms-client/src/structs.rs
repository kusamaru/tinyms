use std::{pin::Pin, task::Poll, path::PathBuf};

use derivative::Derivative;
use futures::{Future, ready};

#[derive(Debug)]
pub struct RunArgs {
    pub host: String,
    pub port: u16,
    pub ask_yes_no: bool,
    pub files: Vec<PathBuf>,
}

#[derive(Debug)]
pub(crate) struct HostFile {
    pub path: PathBuf,
    pub file: makiko::host_file::File,
}

#[derive(Derivative)]
#[derivative(Debug)]
pub(crate) struct Key {
    pub path: PathBuf,
    pub data: Vec<u8>,
    #[derivative(Debug = "ignore")]
    pub decoded: makiko::keys::DecodedPrivkeyNopass,
}

#[derive(Debug)]
pub(crate) struct TaskHandle<T>(pub tokio::task::JoinHandle<T>);

impl<T> Future for TaskHandle<T> {
    type Output = T;
    fn poll(
        self: Pin<&mut Self>, 
        cx: &mut std::task::Context<'_>
    ) -> Poll<Self::Output> {
        match ready!(Pin::new(&mut self.get_mut().0).poll(cx)) {
            Ok(res) => Poll::Ready(res),
            Err(err) if err.is_panic() => std::panic::resume_unwind(err.into_panic()),
            Err(err) => panic!("Task failed: {}", err),
        }
    }
}

impl<T> Drop for TaskHandle<T> {
    fn drop(&mut self) {
        self.0.abort();
    }
}

use makiko::bytes::{BytesMut, BufMut};
use crate::sftp::PacketType;
/// Simple builder to make SFTP packet.
#[derive(Debug)]
pub struct SftpPacket {
    length: u32,
    packet_type: u8,
    payload: BytesMut,
}
impl SftpPacket {
    pub fn new() -> Self {
        SftpPacket { length: 0, packet_type: 0, payload: BytesMut::new() }
    }

    pub fn set_type(&mut self, packet_type: PacketType) {
        self.packet_type = packet_type as u8;
    }

    pub fn get_payload_mut(&mut self) -> &mut BytesMut {
        &mut self.payload
    }

    pub fn finish(mut self) -> makiko::bytes::Bytes {
        self.length = (self.payload.len() + 1) as u32;
        let bytes = self.payload.freeze();
        let mut result = BytesMut::new();
        result.put_u32(self.length);
        result.put_u8(self.packet_type);
        result.put_slice(&bytes);
        result.freeze()
    }
}

#[allow(dead_code)]
pub enum FileType {
    REGULAR = 1,
    DIR = 2,
    UNKNOWN = 5,
}

pub struct FileAttribute {
    pub size: Option<u64>,
    pub file_type: Option<FileType> 
}