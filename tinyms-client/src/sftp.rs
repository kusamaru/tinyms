use anyhow::Result;
use makiko::bytes::BufMut;

use crate::structs::{SftpPacket, FileAttribute, FileType};

/// SSH_FXP_INIT
pub async fn init(session: &makiko::Session) -> Result<()> {
    let mut packet = SftpPacket::new();
    packet.set_type(PacketType::SSH_FXP_INIT);
    packet.get_payload_mut().put_u32(crate::SFTP_PROTOCOL_VERSION);
    session.send_stdin(packet.finish()).await?;
    Ok(())
}

/// SSH_FXP_OPEN
pub async fn open(
    session: &makiko::Session, 
    request_id: u32, 
    filename: String, 
    attrs: FileAttribute
) -> Result<()> {
    let mut packet = SftpPacket::new();
    packet.set_type(PacketType::SSH_FXP_OPEN);
    let payload = packet.get_payload_mut();
    // uint32 request-id
    payload.put_u32(request_id);
    // string filename
    put_string(payload, filename);
    // uint32 desired-access: ace-mask(read,write,append)
    payload.put_u32(0x00000007);
    // uint32 flags: 
    payload.put_u32(0x00000008);
    // ATTRS attrs:
    put_attrs(payload, attrs);

    drop(payload);
    session.send_stdin(packet.finish()).await?;
    Ok(())
}

/// SSH_FXP_CLOSE
pub async fn close(
    session: &makiko::Session,
    request_id: u32,
    handle: String
) -> Result<()> {
    let mut packet = SftpPacket::new();
    packet.set_type(PacketType::SSH_FXP_CLOSE);
    let payload = packet.get_payload_mut();
    // uint32 request-id
    payload.put_u32(request_id);
    // string handle
    put_string(payload, handle);

    drop(payload);
    session.send_stdin(packet.finish()).await?;
    Ok(())
}

/// SSH_FXP_WRITE
pub async fn write(
    session: &makiko::Session, 
    request_id: u32,
    handle: String,
    offset: u64,
    data: Vec<u8>,
) -> Result<()> {
    let mut packet = SftpPacket::new(); 
    packet.set_type(PacketType::SSH_FXP_WRITE);
    let payload = packet.get_payload_mut();
    // uint32 request-id
    payload.put_u32(request_id);
    // string handle
    put_string(payload, handle);
    // uint64 offset
    payload.put_u64(offset);
    // string data
    payload.put_u32(data.len() as u32);
    payload.put_slice(&data);

    drop(payload);
    session.send_stdin(packet.finish()).await?;
    Ok(())
}

/// SSH_FXP_EXTENDED
/// request final path
pub async fn extended(
    session: &makiko::Session,
    request_id: u32,
    handle: String,
) -> Result<()> {
    let mut packet = SftpPacket::new();
    packet.set_type(PacketType::SSH_FXP_EXTENDED);
    let payload = packet.get_payload_mut();
    // uint32 request-id
    payload.put_u32(request_id);
    // string request_type
    put_string(payload, "GET_FILELINK@puage.org".to_string());
    // request-specific data
    put_string(payload, handle);

    drop(payload);
    session.send_stdin(packet.finish()).await?;
    Ok(())
}

#[allow(non_camel_case_types, dead_code)]
pub enum PacketType {
    SSH_FXP_INIT = 1,
    SSH_FXP_VERSION,
    SSH_FXP_OPEN,
    SSH_FXP_CLOSE,
    SSH_FXP_READ,
    SSH_FXP_WRITE,
    SSH_FXP_LSTAT,
    SSH_FXP_FSTAT,
    SSH_FXP_SETSTAT,
    SSH_FXP_FSETSTAT,
    SSH_FXP_OPENDIR,
    SSH_FXP_READDIR,
    SSH_FXP_REMOVE,
    SSH_FXP_MKDIR,
    SSH_FXP_RMDIR,
    SSH_FXP_REALPATH,
    SSH_FXP_STAT,
    SSH_FXP_RENAME,
    SSH_FXP_READLINK,
    SSH_FXP_SYMLINK,
    SSH_FXP_STATUS = 101,
    SSH_FXP_HANDLE,
    SSH_FXP_DATA,
    SSH_FXP_NAME,
    SSH_FXP_ATTRS,
    SSH_FXP_EXTENDED = 200,
    SSH_FXP_EXTENDED_REPLY
}

fn put_string(buf: &mut makiko::bytes::BytesMut, string: String) {
    buf.put_u32(string.len() as u32);
    buf.put_slice(string.as_bytes());
}

fn put_attrs(buf: &mut makiko::bytes::BytesMut, attrs: FileAttribute) {
    buf.put_u32(0x00000001);
    buf.put_u8(attrs.file_type.unwrap_or(FileType::UNKNOWN) as u8);
    buf.put_u64(attrs.size.unwrap_or(0));
}