mod client;
mod structs;
mod sftp;

use clap::Parser;
use serde_derive::{Deserialize, Serialize};
use structs::RunArgs;

const SFTP_PROTOCOL_VERSION: u32 = 3;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[clap(short = 'H', long = "host")]
    host: Option<String>,
    #[clap(short = 'p', long = "port")]
    port: Option<u16>,
    files: Vec<String>,
}

#[tokio::main]
async fn main() {
    env_logger::init();

    let args = Args::parse();
    let default_path = "./.default.json";
    let run_args = {
        if std::path::Path::new(default_path).exists() {
            let def = read_default_file(default_path);
            let files = args.files.iter().map(|s| std::path::Path::new(s).to_path_buf()).collect::<Vec<_>>();
            RunArgs { 
                host: def.host, 
                port: def.port, 
                ask_yes_no: def.ask_yes_no,
                files, 
            }
        } else {
            println!("{:?}", args);
            let host = args.host.expect("parse fail: could not find `--host(-h)` in argument");
            let port = args.port.unwrap_or(22);
            let files = args.files.iter().map(|s| std::path::Path::new(s).to_path_buf()).collect::<Vec<_>>();
            RunArgs { host, port, ask_yes_no: true, files, }
        }
    };

    log::info!("args: {:?}", run_args);
    
    match client::run(run_args).await {
        Ok(_) => {},
        Err(e) => eprintln!("{}", e.to_string()),
    };
}

#[derive(Serialize, Deserialize)]
struct DefaultArgs {
    pub host: String,
    pub port: u16,
    pub ask_yes_no: bool,
}
impl Default for DefaultArgs {
    fn default() -> Self {
        Self { 
            host: String::from("localhost"), 
            port: 22,
            ask_yes_no: true,
        }
    }
}

fn read_default_file(p: &str) -> DefaultArgs {
    let path = std::path::Path::new(p);
    let file = std::fs::File::open(path).unwrap();
    serde_json::from_reader(file).unwrap()
}