# tinyms-client
a simple SFTP client for tinyms.

## Setup
tinyms-client requires:
- .credentials/client_id_ed25519 (privatekey)

## Command-line arguments

## Default
By placing ".default.json" in the same directory as the executable file, you can omit some of the command line arguments at runtime.
The format of ".default.json" is as follows:
```
{
    "host": "localhost", 
    "port": "22",        
    "ask_yes_no": true   // ask about fingerprints
}
```