mod route;
mod web;
pub mod sftp;
mod filetype;
mod env;

use std::ffi::OsStr;
use std::panic::resume_unwind;
use std::{sync::Arc, collections::HashSet};
use anyhow::Result;
use once_cell::sync::Lazy;
use tokio::try_join;
use tokio::{sync::Mutex};

use crate::filetype::FileType;
use crate::env::TraitConfig as _;

static LOCKED_FILES: Lazy<Arc<Mutex<HashSet<String>>>> = Lazy::new(|| Arc::new(Mutex::new(HashSet::new())));
static CONFIG: Lazy<crate::env::Config> = Lazy::new(|| crate::env::Config::new());

pub fn true_temp_path() -> String {
    let local = CONFIG.current_dir().unwrap().to_string_lossy().to_string();
    format!("{}{}", local.replace("\\", "/"), CONFIG.get("TEMP_DIR_PATH").unwrap())
}

// #[tokio::main]
pub async fn entrypoint() -> Result<(), std::io::Error> {
    // initialize
    std::env::set_var("RUST_LOG", "info");
    env_logger::init();

    // setup address
    let (sftp_addr, web_addr) = if std::path::Path::new("/.dockerenv").exists() {
        let sftp_port = CONFIG.get("SFTP_PORT").unwrap_or("2222".to_string());
        let web_port = CONFIG.get("PORT").unwrap_or("3000".to_string());
        log::info!("runmode: release mode");
        (format!("0.0.0.0:{}", sftp_port), format!("0.0.0.0:{}", web_port))
    } else {
        log::info!("runmode: dev mode");
        (String::from("127.0.0.1:2222"), String::from("127.0.0.1:3000"))
    };

    log::info!("sftp server is running on: {}", sftp_addr);
    log::info!(" web server is running on: {}", web_addr);

    // web server
    #[cfg(not(feature="http"))]
    let webs = tokio::spawn(web::run(
        web_addr.clone(), 
        Some(CONFIG.get("CERT").expect("could not find config value CERT.").clone()),
        Some(CONFIG.get("KEY").expect("could not find config value KEY.").clone())
    ));
    #[cfg(feature="http")]
    let webs = tokio::spawn(web::run(web_addr.clone(), None, None));

    // sftp server
    let sftp = tokio::spawn(sftp::run(sftp_addr));

    match try_join!(sftp, webs) {
        Ok(_) => Ok(()),
        Err(e) if e.is_panic() => resume_unwind(e.into_panic()),
        Err(e) => panic!("Unknown Error occured: {}", e.to_string())
    }
}

/// Move a file and return the destination path.
async fn move_file(path: String) -> Result<String, std::io::Error> {
    use std::io::{Error, ErrorKind};

    let Some(n) = std::path::Path::new(&path).file_name() else { 
        log::error!("the requested name is not a file!");
        return Err(Error::new(ErrorKind::NotFound, "unknown file"))
    };
    let name = n.to_string_lossy().to_string();

    let filetype = FileType::match_file_suffix(&name);
    let sub_dir = match filetype.clone() {
        FileType::Data(_) => "data",
        ft if ft.is_media() => "media",
        _ => "data",
    };
    let mut send_to = format!("./files/{}/{}", sub_dir, &name);
    // if send_to is already exist
    if std::path::Path::new(&send_to).exists() {
        for index in 1..100 {
            let entry_path = std::path::Path::new(&path);
            send_to = format!(
                "./files/{}/{}_{}.{}", 
                sub_dir, 
                entry_path.file_stem().unwrap_or(OsStr::new("unknown")).to_string_lossy().to_string(),
                index,
                get_suffix(entry_path.file_name().unwrap_or(OsStr::new("unknown")).to_string_lossy().to_string()),
            );

            if std::path::Path::new(&send_to).exists() { 
                continue;
            } else {
                tokio::fs::copy(path.clone(), send_to.clone()).await?;
                break;
            }
        }
    } else {
        tokio::fs::copy(path.clone(), send_to.clone()).await?;
    }
    tokio::fs::remove_file(path).await?;

    if matches!(filetype, FileType::Image(_) | FileType::Video(_)) {
        let numbered = format!("./files/{}/{}", sub_dir, numbering(&name));
        match tokio::fs::rename(
            format!("./files/{}/{}", sub_dir, &name), 
            numbered.clone()
        ).await {
            Ok(_) => send_to = numbered,
            Err(_) => {}, // error handling
        }
    }

    Ok(send_to)
}

fn numbering(filename: &str) -> String {
    let uuid = uuid::Uuid::new_v4().as_simple().to_string();
    let suffix = get_suffix(filename);
    format!(
        "{}{}{}", 
        uuid,  
        if !suffix.is_empty() { "." } else { "" },
        suffix,
    )
}

fn get_suffix<S: AsRef<str>>(filename: S) -> String {
    if !filename.as_ref().contains(".") { return String::new() }
    filename.as_ref().split(".").collect::<Vec<&str>>().pop().unwrap_or("").to_string().to_lowercase()
}

/*
    Tests
*/

#[test]
fn test_true_temp_path() {
    assert!(true_temp_path().contains(CONFIG.current_dir().unwrap().to_string_lossy().to_string().as_str()));
}

#[test]
fn test_numbering() {
    assert_ne!(
        numbering("simple_file.txt"),
        "simple_file.txt".to_string()
    );
    assert_ne!(
        numbering("nosuffix"),
        "nosuffix".to_string()
    );
    let numbered = numbering("this.file.name.contains.many.dots.mp4");
    assert!(numbered.ends_with("mp4"));
    assert_ne!(
        numbered,
        "this.file.name.contains.many.dots.mp4".to_string()
    )
}