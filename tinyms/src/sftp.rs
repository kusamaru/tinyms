use async_trait::async_trait;
use bytes::{Buf, Bytes, BytesMut, BufMut};
use log::{info, error, warn};
use russh_sftp::protocol::{StatusCode, Status, Handle, Name, FileAttributes, File, OpenFlags, Data, Attrs, Version, ExtendedReply};
use std::sync::{Mutex, Arc};
use std::time::Duration;
use russh::*;
use russh::server::{Auth, Session, Msg};
use std::collections::HashMap;

use crate::CONFIG;
use crate::env::TraitConfig as _;

pub async fn run(address: String) {
    let pubkey_path = CONFIG.get("CLIENT_PUBKEY_PATH").expect("cannot find env $CLIENT_PUBKEY_PATH");
    let client_pubkey = Arc::new(russh_keys::load_public_key(pubkey_path).expect("cannot find client public key on $CLIENT_PUBKEY_PATH"));
    let server_keys = {
        let pr_path = CONFIG.get("SERVER_PRVKEY_PATH").expect("cannot find env $SERVER_PRVKEY_PATH");
        let passphrase = CONFIG.get("SERVER_KEY_PASS").expect("cannot find env $SERVER_KEY_PASS");
        let pr = russh_keys::load_secret_key(
            pr_path, 
            if passphrase.is_empty() { None } else { Some(&passphrase) }
        ).expect("cannot find server public key on $SERVER_PRVKEY_PATH");
        pr
    };

    let config = Arc::new(server::Config {
        auth_rejection_time: Duration::from_secs(3), 
        connection_timeout: Some(Duration::from_secs(1800)),
        keys: vec![server_keys],
        ..Default::default()
    });
    let sh = Server {
        client_pubkey,
        clients: Arc::new(Mutex::new(HashMap::new())),
        id: 0
    };

    russh::server::run(config, address, sh).await.unwrap();
}

#[derive(Clone)]
struct Server {
    client_pubkey: Arc<russh_keys::key::PublicKey>,
    clients: Arc<Mutex<HashMap<ChannelId, Channel<Msg>>>>,
    id: usize,
}

impl server::Server for Server {
    type Handler = Self;
    fn new_client(&mut self, _: Option<std::net::SocketAddr>) -> Self {
        let s = self.clone();
        self.id += 1;
        s
    }
}

#[async_trait]
impl server::Handler for Server {
    type Error = anyhow::Error;

    async fn channel_open_session(
        self, 
        channel: Channel<Msg>, 
        session: Session
    ) -> Result<(Self, bool, Session), Self::Error> {
        {
            let mut clients = self.clients.lock().unwrap();
            clients.insert(channel.id(), channel);
        }
        Ok((self, true, session))
    }

    async fn auth_password(self, _user: &str, _password: &str) -> Result<(Self, Auth), Self::Error> {
        Ok((self, server::Auth::Reject { proceed_with_methods: None }))
    }

    async fn auth_publickey(
        self, 
        _name: &str, 
        publickey: &russh_keys::key::PublicKey
    ) -> Result<(Self, Auth), Self::Error> {
        let key = self.client_pubkey.as_ref();
        if key != publickey { return Ok((self, server::Auth::Reject { proceed_with_methods: None })); }

        info!("publickey auth: success");
        Ok((self, server::Auth::Accept))
    }

    async fn subsystem_request(
        mut self,
        channel_id: ChannelId,
        name: &str,
        mut session: Session,
    ) -> Result<(Self, Session), Self::Error> {
        info!("subsystem: {}", name);

        if name == "sftp" {
            let channel = self.clients.lock().unwrap().remove(&channel_id).unwrap();
            let sftp = SftpSession::default();
            session.channel_success(channel_id);
            russh_sftp::server::run(channel.into_stream(), sftp).await;
        } else {
            session.channel_failure(channel_id);
        }

        Ok((self, session))
    }
}

struct SftpSession {
    version: Option<u32>,
    is_dir_readed: bool,
    uploaded_file: Arc<Mutex<HashMap<String, String>>>
}

impl Default for SftpSession {
    fn default() -> Self {
        Self {
            version: None,
            is_dir_readed: false,
            uploaded_file: Arc::new(Mutex::new(HashMap::new()))
        }
    }
}

fn status_ok(id: u32) -> Status {
    Status {
        id,
        status_code: StatusCode::Ok,
        error_message: String::from("Ok"),
        language_tag: String::from("en-US"),
    }
}

#[async_trait]
impl russh_sftp::server::Handler for SftpSession {
    type Error = StatusCode;

    fn unimplemented(&self) -> Self::Error {
        StatusCode::OpUnsupported
    }

    async fn init(
        &mut self,
        version: u32,
        extensions: HashMap<String, String>,
    ) -> Result<Version, Self::Error> {
        if self.version.is_some() {
            error!("SSH_FXP_VERSION dup");
            return Err(StatusCode::ConnectionLost);
        }

        self.version = Some(version);
        info!("version: {:?}, extensions: {:?}", self.version, extensions);
        Ok(Version::new())
    }

    async fn open(
        &mut self,
        id: u32,
        filename: String,
        _pflags: OpenFlags,
        _attrs: FileAttributes,
    ) -> Result<Handle, Self::Error> {
        let mut lock_list = crate::LOCKED_FILES.lock().await;
        lock_list.insert(filename.clone());
        info!("open: insert success, {}", &filename);
        Ok(Handle { id, handle: filename })
    }

    async fn read(
        &mut self,
        id: u32,
        handle: String,
        offset: u64,
        len: u32,
    ) -> Result<Data, Self::Error> {
        use tokio::io::{AsyncReadExt, AsyncSeekExt};
        
        info!("read: id = {}, handle = {}, offset = {}, len = {}", id, handle, offset, len);

        async fn read_to(file: &mut tokio::fs::File, len: u32) -> Option<Vec<u8>> {
            let mut buf: Vec<u8> = Vec::new();
            
            let mut index: usize = 0;
            while index < len as usize {
                let mut b = vec![0u8; (len - index as u32) as usize];
                let result = file.read(&mut b).await;
                match result {
                    Ok(0) => { // EOF or b.len = 0
                        break;
                    },
                    Ok(n) => { // read N byte(s)
                        let tmp = &b[0..n];
                        buf.append(&mut tmp.to_vec());
                        index += n;
                    },
                    Err(_) => return None,
                }
            }
            if index == 0 { 
                // index = len = 0
                return None 
            }
            if index < len as usize {
                // maybe file last part(buffer was not filled)
                let tmp = &buf[0..index];
                Some(tmp.to_vec())
            } else {
                Some(buf)
            }
        }

        let Ok(mut raw_file) = tokio::fs::File::open(&handle).await else { return Err(StatusCode::NoSuchFile) };
        let _seek_result = raw_file.seek(std::io::SeekFrom::Start(offset)).await;
        let data = read_to(&mut raw_file, len).await;
        if data.is_none() {
            return Err(StatusCode::Eof);
        }

        info!("read: ok!");
        let true_data = data.unwrap();
        if true_data.iter().all(|x| x == &0x00) { warn!("read: is 0x00!"); }
        Ok(Data {
            id,
            data: true_data,
        })
    }

    async fn write(
        &mut self,
        id: u32,
        handle: String,
        offset: u64,
        data: Vec<u8>,
    ) -> Result<Status, Self::Error> {
        use tokio::io::{AsyncWriteExt, AsyncSeekExt};
        info!("write: id = {}, handle = {}, offset = {}", id, handle, offset);

        let Ok(mut raw_file) = create_file(&handle).await else { return Err(StatusCode::NoSuchFile) };
        let _seek_result = raw_file.seek(std::io::SeekFrom::Start(offset)).await;
        if let Err(_) = raw_file.write_all(&data).await { return Err(StatusCode::Failure) }
        Ok(status_ok(id))
    }

    async fn rename(
        &mut self,
        id: u32,
        oldpath: String,
        newpath: String,
    ) -> Result<Status, Self::Error> {
        let result = tokio::fs::rename(
            map_path(oldpath), 
            map_path(newpath)
        ).await;
        match result {
            Ok(_) => {
                Ok(status_ok(id))
            },
            Err(_) => {
                Err(StatusCode::Failure)
            }
        }
    }

    async fn fstat(&mut self, id: u32, handle: String) -> Result<Attrs, Self::Error> {
        let Ok(raw_file) = create_file(&handle).await else { return Err(StatusCode::NoSuchFile) };
        let Ok(metadata) = raw_file.metadata().await else { return Err(StatusCode::Failure) };
        Ok(Attrs {
            id,
            attrs: FileAttributes::from(&metadata)
        })
    }

    async fn setstat(
        &mut self,
        _id: u32,
        path: String,
        attrs: FileAttributes,
    ) -> Result<Attrs, Self::Error> {
        let Ok(raw_file) = tokio::fs::OpenOptions::new().read(true).write(true).open(map_path(path.clone())).await else { return Err(StatusCode::NoSuchFile) };
        let a = if let Some(a) = attrs.atime { 
            Some(filetime::FileTime::from_unix_time(a as i64, 0)) 
        } else { 
            None 
        };
        let m = if let Some(m) = attrs.mtime {
            Some(filetime::FileTime::from_unix_time(m as i64, 0)) 
        } else {
            None
        };
        let result = filetime::set_file_handle_times(
            &raw_file.into_std().await, 
            a,
            m,
        );

        let Ok(rf) = tokio::fs::File::open(map_path(path.clone())).await else { return Err(StatusCode::NoSuchFile) };
        info!("setstat: {:?}", rf);
        match result {
            Ok(_) => {
                Err(StatusCode::Ok)
            },
            Err(e) => {
                error!("{}", e.to_string());
                error!("setstat fail");
                Err(StatusCode::Failure)
            }
        }
    }

    async fn fsetstat(
        &mut self,
        id: u32,
        handle: String,
        attrs: FileAttributes,
    ) -> Result<Attrs, Self::Error> {
        let Ok(raw_file) = create_file(&handle).await else { return Err(StatusCode::NoSuchFile) };
        let a = if let Some(a) = attrs.atime { 
            Some(filetime::FileTime::from_unix_time(a as i64, 0)) 
        } else { 
            None 
        };
        let m = if let Some(m) = attrs.mtime {
            Some(filetime::FileTime::from_unix_time(m as i64, 0)) 
        } else {
            None
        };
        let result = filetime::set_file_handle_times(
            &raw_file.into_std().await, 
            a,
            m,
        );

        let Ok(rf) = open_file(&handle).await else { return Err(StatusCode::NoSuchFile) };
        info!("fsetstat: {:?}", rf);
        match result {
            Ok(_) => {
                Ok(Attrs {
                    id,
                    attrs: FileAttributes::from(&rf.metadata().await.unwrap())
                })
            },
            Err(_) => {
                Err(StatusCode::Failure)
            }
        }
    }

    async fn close(
        &mut self,
        id: u32,
        handle: String
    ) -> Result<Status, Self::Error> {
        let mut lock_list = crate::LOCKED_FILES.lock().await;
        let remove_result = lock_list.remove(&handle);
        info!("close: remove_result = {}, remove target = {}", remove_result, &handle);
        match crate::move_file(map_path(&handle)).await {
            Err(e) => panic!("{}", e.to_string()), // todo
            Ok(name) => {
                self.uploaded_file.lock().unwrap().insert(handle, name);
            },
        }
        Ok(status_ok(id))
    }

    async fn opendir(
        &mut self,
        id: u32,
        path: String
    ) -> Result<Handle, Self::Error> {
        info!("opendir: {}", path);
        self.is_dir_readed = false;
        Ok(Handle { id, handle: path })
    }

    async fn readdir(
        &mut self,
        id: u32,
        handle: String,
    ) -> Result<Name, Self::Error> {
        info!("readdir: {}", handle);
        if !self.is_dir_readed {
            self.is_dir_readed = true;
            
            let files = read_dir(&handle).await?;
            return Ok(Name {
                id,
                files,
            });
        }
        Ok(Name {
            id,
            files: vec![],
        })
    }

    async fn realpath(
        &mut self,
        id: u32,
        path: String,
    ) -> Result<Name, Self::Error> {
        info!("realpath: {}", path);
        Ok(Name {
            id,
            files: vec![File {
                filename: path,
                attrs: FileAttributes::default(),
            }]
        })
    }

    async fn extended(
        &mut self,
        id: u32,
        request: String,
        data: Vec<u8>,
    ) -> Result<ExtendedReply, Self::Error> {
        info!("extended: {}", request);
        match request.as_str() {
            "GET_FILELINK@puage.org" => {
                let mut bytes = Bytes::from_iter(data.iter().map(|d| *d));
                let handle = {
                    let len = bytes.get_u32() as usize;
                    let Some(raw) = bytes.get(0..len) else { return Err(StatusCode::BadMessage) };
                    String::from_utf8_lossy(raw).to_string()
                };

                let mut list = self.uploaded_file.lock().unwrap();
                let Some(name) = list.remove(&handle) else { return Err(StatusCode::Failure) };
                
                let mut resp = BytesMut::new();
                resp.put_u32("GET_FILELINK".len() as u32);
                resp.put_slice("GET_FILELINK".as_bytes());
                resp.put_u32(name.len() as u32);
                resp.put_slice(name.as_bytes());
                Ok(ExtendedReply { 
                    id, 
                    data: resp.freeze().to_vec()
                })
            },
            _ => Err(self.unimplemented())
        }
    }
}

/// map '/hoge.txt' to '{$PATH_TO_EXECUTABLE}/{crate::TEMPDIR_PATH}/hoge.txt'
fn map_path<S: AsRef<str>>(path: S) -> String {
    let path_str = path.as_ref();
    let mapped = format!("{}{}", crate::true_temp_path(), path_str);
    info!("map_path: {}", &mapped);
    mapped
} 

async fn open_file<S: AsRef<std::ffi::OsStr> + ?Sized>(
    s: &S
) -> Result<tokio::fs::File, anyhow::Error> {
    use tokio::fs::OpenOptions;

    let mapped = map_path(s.as_ref().to_string_lossy().to_string());
    let file = OpenOptions::new().read(true).open(mapped).await.unwrap();
    info!("open file len: {}", file.metadata().await.unwrap().len());
    Ok(file)
}
async fn create_file<S: AsRef<std::ffi::OsStr> + ?Sized>(
    s: &S
) -> Result<tokio::fs::File, anyhow::Error> {
    use tokio::fs::OpenOptions;

    let mapped = map_path(s.as_ref().to_string_lossy().to_string());
    let file = OpenOptions::new().create(true).write(true).read(true).open(mapped).await.unwrap();
    info!("create file len: {}", file.metadata().await.unwrap().len());
    Ok(file)
}

async fn read_dir<S: AsRef<std::ffi::OsStr> + ?Sized>(
    s: &S
) -> Result<Vec<File>, StatusCode> {
    let mapped = map_path(s.as_ref().to_string_lossy().to_string());
    let Ok(mut reader) = tokio::fs::read_dir(mapped).await else { return Err(StatusCode::NoSuchFile) };

    let mut result: Vec<File> = Vec::new();
    while let Ok(Some(entry)) = reader.next_entry().await {
        let Ok(mt) = entry.metadata().await else { continue; };
        let mut attrs = FileAttributes::default();
        attrs.size = Some(mt.len());
        attrs.set_dir(mt.is_dir());
        attrs.set_symlink(mt.is_symlink());
        let file = File {
            filename: entry.file_name().to_string_lossy().to_string(),
            attrs,
        };
        result.push(file);
    }
    Ok(result)
}

/*
    Tests
*/


#[tokio::test(flavor = "multi_thread", worker_threads = 2)]
async fn test_run() {
    // error
    std::net::TcpStream::connect("127.0.0.1:2222").unwrap_err();

    // ok
    tokio::spawn(run("127.0.0.1:2222".to_string()));
    std::net::TcpStream::connect("127.0.0.1:2222").unwrap();
}

#[test]
fn test_map_path() {
    assert_eq!(
        map_path("/test_transporting.mp4"),
        "/test_dir/tmp/test_transporting.mp4".to_string()
    );
    assert_eq!(
        map_path("/dir/dir2/fugaaaa.wav"),
        "/test_dir/tmp/dir/dir2/fugaaaa.wav".to_string()
    );
}