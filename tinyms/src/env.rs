#![allow(dead_code)]

use anyhow::Result;
use std::{collections::HashMap, fs::File, path::PathBuf};

const DEFAULT_CONFIG_PATH: &str = ".config";

pub trait TraitConfig {
    fn new() -> Self where Self: Sized;
    fn get<S: AsRef<str>>(&self, key: S) -> Option<String>;

    fn current_dir(&self) -> Result<std::path::PathBuf, std::io::Error> {
        std::env::current_dir()
    }
}

pub struct Config {
    config: HashMap<String, String>
}

#[cfg(not(test))]
impl TraitConfig for Config {
    fn new() -> Self {
        let path = std::path::Path::new(DEFAULT_CONFIG_PATH).to_path_buf();
        let config: HashMap<String, String> = parse_config(path.clone()).expect("parse fail");
        Config { config }
    }

    fn get<S: AsRef<str>>(&self, key: S) -> Option<String> {
        self.config.get(key.as_ref()).cloned()
    }
}             

#[cfg(test)]
impl TraitConfig for Config {
    fn new() -> Self { Config { config: HashMap::new() } }

    fn get<S: AsRef<str>>(&self, key: S) -> Option<String> {
        match key.as_ref() {
            // const
            "TEMP_DIR_PATH" => Some("/tmp".to_string()),
            "CLIENT_PUBKEY_PATH" => Some("./tests/credentials/test_client_key.pub".to_string()),
            "SERVER_PRVKEY_PATH" => Some("./tests/credentials/test_server_key".to_string()),
            "SERVER_KEY_PASS" => Some("".to_string()),
            // others
            _ => self.config.get(key.as_ref()).cloned()
        }
    }
    
    fn current_dir(&self) -> Result<std::path::PathBuf, std::io::Error> {
        Ok(std::path::Path::new("/test_dir").to_path_buf())
    }
}

fn parse_config(path: PathBuf) -> Result<HashMap<String, String>> {
    use std::io::{BufReader, BufRead};

    let reader = BufReader::new(File::open(path)?);
    let mut lines = reader.lines().map(|l| l.unwrap());
    let mut result: HashMap<String, String> = HashMap::new();
    while let Some(line) = lines.next() {
        match line {
            l if l.starts_with("#") => continue, // comment
            l if l.contains("=") => {
                let split = l.split("=").collect::<Vec<&str>>();
                if split.len() < 2 { anyhow::bail!("no key or value specified: {:?}", split) }
                let key = split.get(0).expect("key is invalid").to_string();
                let value = split.get(1..).expect("value is invalid").concat();
                result.insert(key, value);
            },
            _ => {}
        }
    }

    // default values
    if_none_insert(&mut result, "CLIENT_PUBKEY_PATH", "./.credentials/client_id_ed25519.pub");
    if_none_insert(&mut result, "SERVER_PRVKEY_PATH", "./.credentials/server_id_ed25519");
    if_none_insert(&mut result, "SERVER_KEY_PASS", "");
    if_none_insert(&mut result, "TEMP_DIR_PATH", "/tmp");
    Ok(result)
}
fn if_none_insert(l: &mut HashMap<String, String>, k: &str, v: &str) {
    if l.get(k).is_none() { l.insert(k.into(), v.into()); }
}

#[test]
fn test_env() {
    let env = Config::new();

    assert_ne!(
        env.current_dir().unwrap(),
        std::env::current_dir().unwrap()
    );
}