use tokio::{fs::File, io::BufReader};
use poem::{handler, web::Path, IntoResponse, Response, Body, Server, listener::{Listener, TcpListener, RustlsCertificate, RustlsConfig}};

use crate::FileType;

pub async fn run(addr: String, cert_path: Option<String>, key_path: Option<String>) -> Result<(), std::io::Error> {
    let app = crate::route::route();

    #[cfg(not(feature="http"))]
    let listener = {
        log::info!("cert: {:?}, key: {:?}", cert_path, key_path);
        let cert = std::fs::read(cert_path.unwrap()).unwrap();
        let key = std::fs::read(key_path.unwrap()).unwrap();
        TcpListener::bind(addr)
            .rustls(RustlsConfig::new()
                .fallback(RustlsCertificate::new()
                    .key(key)
                    .cert(cert)
                )
            )
    };
    #[cfg(feature="http")]
    let listener = TcpListener::bind(addr);

    Server::new(listener)
        .name("tinyms")
        .run(app)
        .await
}

fn not_found() -> Response {
    Response::builder()
        .content_type("text/plain")
        .body("not found")
}

#[handler]
pub async fn media(Path(path): Path<String>) -> Response {
    let Ok(file) = get_file(&format!("./files/media/{}", path)).await else { return not_found() };
    let filetype = FileType::match_file_suffix(&path);
    let (discrete, subtype) = filetype.as_media_type();
    println!("file: {}, {}/{}", path, discrete, subtype);
    let reader = BufReader::new(file);
    Response::builder()
        .content_type(format!("{}/{}", discrete, subtype))
        .body(Body::from_async_read(reader))
}

#[handler]
pub async fn data(Path(path): Path<String>) -> impl IntoResponse {
    let Ok(file) = get_file(&format!("./files/data/{}", path)).await else { return not_found() };
    let filetype = FileType::match_file_suffix(&path);
    let (discrete, subtype) = filetype.as_media_type();
    let reader = BufReader::new(file);
    Response::builder()
        .content_type(format!("{}/{}", discrete, subtype))
        .body(Body::from_async_read(reader))
}

async fn get_file(path: &String) -> anyhow::Result<File> {
    let p = std::path::Path::new(path);
    let result = tokio::fs::OpenOptions::new().read(true).open(p).await;
    match result {
        Ok(file) => {
            Ok(file)
        },
        Err(e) => {
            log::warn!("file not found! {}", path);
            Err(e.into())
        }
    }
}