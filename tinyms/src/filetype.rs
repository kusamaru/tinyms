#[derive(Debug, Clone, PartialEq)]
pub enum FileType {
    Nil,
    Audio(String),
    Image(String),
    Video(String),
    Data(String),
}

impl FileType {
    pub fn match_file_suffix<S: AsRef<str>>(name: S) -> Self {
        let suffix = crate::get_suffix(name);
        // todo! refactor
        #[allow(unused_parens)]
        match suffix.as_str() {
            "" => Self::Nil,
            mut im @ ("jpg" | "jpeg" | "png" | "gif") => {
                if im == "jpg" { im = "jpeg" };
                Self::Image(im.to_string())
            },
            au @ ("wav" | "mp3" | "ogg") => {
                Self::Audio(au.to_string())
            },
            vi @ ("mp4") => {
                Self::Video(vi.to_string())
            }
            // unknown
            x => Self::Data(x.to_string()),
        }
    }

    pub fn as_media_type(self) -> (&'static str, String) {
        match self {
            FileType::Audio(audio) => {
                ("audio", audio)
            },
            FileType::Image(image) => {
                ("image", image)
            },
            FileType::Video(video) => {
                ("video", video)
            },
            FileType::Data(_data) => {
                // if _data == "" {
                //     ("application", String::from("octet-stream"))
                // } else {
                //     ("application", _data)
                // }
                ("application", String::from("octet-stream"))
            },
            FileType::Nil => {
                ("application", String::from("octet-stream"))
            }
        }
    }
    
    pub fn is_media(&self) -> bool {
        match self {
            FileType::Audio(_) |
            FileType::Image(_) |
            FileType::Video(_) => true,
            _ => false,
        }
    }
}

/*
    Test
*/

#[test]
fn test_match_file_suffix() {
    assert_eq!(
        FileType::match_file_suffix(""),
        FileType::Nil,
    );
    assert_eq!(
        FileType::match_file_suffix("config."),
        FileType::Nil,
    );
    assert_eq!(
        FileType::match_file_suffix(".some_credentials."),
        FileType::Nil,
    );
    assert_eq!(
        FileType::match_file_suffix("Have a nice space."),
        FileType::Nil,
    );
    assert_eq!(
        FileType::match_file_suffix("vimrc"),
        FileType::Nil,
    );
    assert_eq!(
        FileType::match_file_suffix("２バイト文字万歳！"),
        FileType::Nil,
    );

    assert_eq!(
        FileType::match_file_suffix("２バイト拡張子.万歳"),
        FileType::Data("万歳".to_string()),
    );
    assert_eq!(
        FileType::match_file_suffix(".gitignore"),
        FileType::Data("gitignore".to_string()),
    );
    assert_eq!(
        FileType::match_file_suffix("hogehoge.txt"),
        FileType::Data("txt".to_string()),
    );
    assert_eq!(
        FileType::match_file_suffix("UPPERCASE.TXT"),
        FileType::Data("txt".to_string()),
    );
    assert_eq!(
        FileType::match_file_suffix("hogehoge.txt.exe"),
        FileType::Data("exe".to_string()),
    );

    assert_eq!(
        FileType::match_file_suffix("osakafu.jpg"),
        FileType::Image("jpeg".to_string()),
    );
    assert_eq!(
        FileType::match_file_suffix("chibaken.jpeg"),
        FileType::Image("jpeg".to_string()),
    );
    assert_eq!(
        FileType::match_file_suffix("kyotofu.png"),
        FileType::Image("png".to_string()),
    );
    assert_eq!(
        FileType::match_file_suffix("gifuken.gif"),
        FileType::Image("gif".to_string()),
    );

    assert_eq!(
        FileType::match_file_suffix("somebody_scream.mp3"),
        FileType::Audio("mp3".to_string()),
    );
    assert_eq!(
        FileType::match_file_suffix("lucky_comes_true!.wav"),
        FileType::Audio("wav".to_string()),
    );
    assert_eq!(
        FileType::match_file_suffix("maybe_nice_song.ogg"),
        FileType::Audio("ogg".to_string()),
    );

    assert_eq!(
        FileType::match_file_suffix("syusaku.mp4"),
        FileType::Video("mp4".to_string()),
    );
}

#[test]
fn test_as_media_type() {
    assert_eq!(
        FileType::match_file_suffix("").as_media_type(),
        ("application", String::from("octet-stream"))
    );
    assert_eq!(
        FileType::match_file_suffix("this-file-is-todo").as_media_type(),
        ("application", String::from("octet-stream"))
    );
    assert_eq!(
        FileType::match_file_suffix("0001.txt").as_media_type(),
        ("application", String::from("octet-stream"))
    );
    assert_eq!(
        FileType::match_file_suffix("rashomon-the-movie.mp4").as_media_type(),
        ("video", String::from("mp4"))
    );
    assert_eq!(
        FileType::match_file_suffix("insane_dj_meme.gif").as_media_type(),
        ("image", String::from("gif"))
    );
    assert_eq!(
        FileType::match_file_suffix("surround.wav").as_media_type(),
        ("audio", String::from("wav"))
    );
}