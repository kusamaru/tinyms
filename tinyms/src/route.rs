use poem::{Route, handler, web::Html, get};

#[handler]
fn index() -> Html<String> {
    Html(String::from("tiny media server"))
}

#[handler]
fn unimplemented() -> Html<String> {
    Html(String::from("unimplemented path"))
}

pub fn route() -> Route {
    Route::new()
        .at("/", get(index))
        .nest("/data", get(unimplemented))
        .nest("/media", get(unimplemented))
        .nest("/data/:path", get(crate::web::data))
        .nest("/media/:path", get(crate::web::media))
}