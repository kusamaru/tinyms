# tinyms
A project of tiny media server written in Rust

## Setup
tinyms requires ENV vars:
- $SERVER_PRVKEY_PATH: uses to get server private key(ed25519).     default = ./.credentials/server_id_ed25519
- $SERVER_KEY_PASS:    server key passphrase. if none, MUST be "".  default = ""
- $CLIENT_PUBKEY_PATH: uses to get client public key(ed25519).      default = ./.credentials/client_id_ed25519.pub

tinyms requires the cert/key file to be specified in .config.
```
# comment.
CERT=/path/to/cert/cert.pem
KEY=/path/to/key/privkey.pem
```

## Features
- http