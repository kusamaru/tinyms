#!/bin/sh

# exit if argument is none
if [ $# == 0 ]; then
    echo "Error: No command-line argument was taken. Please specify send file."
    read -p "Press [Enter] key to exit: "
    exit
fi

(
    cd ./tinyms-client
    cargo run -- ${@:1}
)